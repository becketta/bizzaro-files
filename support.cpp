/* ------------------------------------------------------------------
 * file name: support.cpp
 *
 *      Defines custom functions to be used by
 *      flipflopper.cpp, otherwise known as Bizzaro.
 *
 * author: Aaron Beckett
 *   date: 04/21/2015
 * ------------------------------------------------------------------
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>    // for reverse function
#include <sys/stat.h>   // for 'stat' system call
#include <sys/types.h>
#include <dirent.h>     // for 'dirent' struct and readdir system call
#include <memory>
#include <unistd.h>     // for the 'access' system call

#include "support.h"

using namespace std;

/*
 * Function name: dinvert
 *      Creates a bizzaro directory from the given directory by inverting
 *      the names and contents of regular files and names of directories.
 *
 * dirname: Name of the directory to invert
 * loc: Directory in which to place the final bizzaro directory
 *
 * returns: true if successful
 */
bool dinvert(const string &dirname)
{
    DIR *dir;
    struct dirent *entry;
    string sep = "/";

    errno = 0;
    dir = opendir(dirname.c_str());
    if (errno != 0) {
        cout << "opendir error. " << strerror(errno) << endl;
        return false;
    }
    else if (dir) {
        errno = 0;
        string entry_path;
        string bizzname;
        vector<shared_ptr<swapPair> > name_swaps;
        bool mirrored = false;

        while((entry = readdir(dir)) != NULL) {
            entry_path = entry->d_name;
            if (entry_path != "." && entry_path != "..") {
                switch(entry->d_type) {
                    case DT_REG:
                        bizzname = dirname + sep + sinvert(entry_path);
                        entry_path = dirname + sep + entry_path;
                        name_swaps.push_back(make_shared<swapPair>(entry_path, bizzname));
                        finvert(entry_path);
                        break;
                    case DT_DIR:
                        bizzname = dirname + sep + sinvert(entry_path);
                        entry_path = dirname + sep + entry_path;
                        name_swaps.push_back(make_shared<swapPair>(entry_path, bizzname, canAccess(bizzname)));
                        dinvert(entry_path);
                        break;
                }
            }
        }
        if (errno != 0) {
            cout << "readdir error. " << strerror(errno) << endl;
            return false;
        }

        closedir(dir);

        vector<string> mirrors;
        string temp;
        bool handled = false;
        for (vector<shared_ptr<swapPair> >::iterator it = name_swaps.begin(); it != name_swaps.end(); ++it) {
            if ( (*it)->mirrored ) {
                // If we already handled this pair, continue
                handled = false;
                for (int i=0; i < mirrors.size(); i++)
                    if ((*it)->new_name == mirrors[i]) { handled = true; }
                // Else process this new mirrored pair
                if (!handled) {
                    errno = 0;
                    temp = (*it)->old_name + ".IMsureTHISwillNEVERbeUSED";
                    rename( (*it)->old_name.c_str(), temp.c_str());
                    rename( (*it)->new_name.c_str(), (*it)->old_name.c_str());
                    rename( temp.c_str(), (*it)->new_name.c_str());
                    if (errno != 0) { handle_error(errno, "rename"); }
                    mirrors.push_back( (*it)->old_name );
                }
            }
            else {
                errno = 0;
                rename( (*it)->old_name.c_str(), (*it)->new_name.c_str() );
                if (errno != 0) { handle_error(errno, "rename"); }
            }
        }
    }

    return true;
}

/*
 * Function name: finvert
 *      Creates a bizzaro file from the given file and places it in the 
 *      specified location.
 *
 * filename: Name of the file to invert
 * loc: Directory in which to place the final inverted file
 *
 * returns: true if successful
 */
bool finvert(const string &filename)
{
    fstream fs(filename);
    string content;

    if (fs.is_open())
    {
        // Get total length of file and create buffer
        fs.seekg(0, fs.end);
        int len = fs.tellg();
        fs.seekg(0, fs.beg);
        vector<char> buffer(len);

        // Read contents into buffer and reverse buffer
        fs.read(&buffer[0], len);
        if (fs.fail()) {
            cout << "Unable to read " << filename << endl;
            return false;
        }
        else {
            reverse(buffer.begin(), buffer.end());
        }

        // Overwrite the file with the reversed buffer
        fs.seekg(0, fs.beg);
        fs.write(&buffer[0], len);
        if (fs.fail())
        {
            cout << "Unable to create bizzaro file from " << filename << endl;
            return false;
        }

        fs.close();
    }
    else
    {
        cout << "Unable to open filestream to " << filename << ".\n";
        return false;
    }

    return true;
}

/*
 * Function name: filetype
 *      Returns the type of file referenced by the given file name.
 */
typ filetype(const string &filename)
{
    typ t = NONE;
    
    errno = 0;
    struct stat info;
    stat(filename.c_str(), &info);
    if (errno != 0) {
        t = NONE;
    }
    else if (S_ISREG(info.st_mode)) {
        t = REGULAR_FILE;
    }
    else if (S_ISDIR(info.st_mode)) {
        t = DIRECTORY;
    }
    else {
        t = OTHER;
    }

    return t;
}

/*
 * Function name: sinvert
 *      Inverts the given string of characters.
 *
 * original: String to invert
 * returns: A string that is the original backwards.
 */
string sinvert(const string &original)
{
    string rev = original;
    reverse(rev.begin(), rev.end());
    return rev; 
}

/*
 * Function name: getFileName
 */
string getFileName(const string &path)
{
    string fname = path;
    char sep = '/';
    int i = fname.rfind(sep, fname.size()-2);
    fname = fname.substr(i+1);
    if (fname[fname.size()-1] == sep)
        fname.erase(fname.size()-1);
    return fname;
}

/*
 * Function name: getBasePath
 */
string getBasePath(const string &path)
{
    string base = path;
    char sep = '/';
    int i = base.rfind(sep, base.size()-2);
    return base.substr(0,i+1);
}

/*
 * Function name: canAccess
 */
bool canAccess(const string &path)
{
    errno = 0;
    access(path.c_str(), F_OK);
    if (errno == 0) {
        return true;
    }
    else {
        return false;
    }
}

