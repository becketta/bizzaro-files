#-------------------------------------------------------------------#
# CSE 410 Project 3 README                         Aaron Beckett    #
#-------------------------------------------------------------------#

Student NetID: becketta
I have compiled, run, and tested my code on ned.

NOTES:
    I use the stat system call to check the type of file passed as 
    an argument to the flipflopper. To check for regular files I use
    the S_ISREG function that comes with sys/stat.h and is the
    recommended way to check the file type. 

    One caveat of using S_ISREG is that it cannot differentiate 
    between regular files and symbolic links to regular files. The
    program will still work if you pass a symbolic link, it will just
    create the bizzaro file with the inverted name of the link and
    the inverted contents of the regular file pointed to by the link.

USAGE:
    $ make -f Makefile
    $ ./proj3 <file> | <directory>


SAMPLE OUTPUT:

<ned:~ >make -f Makefile
<ned:~ >
<ned:~ >
<ned:~ >ls
proj3  test  test.txt
<ned:~ >cat test.txt
My lady Buchanan.

What up my main man?
<ned:~ >./proj3 test.txt
<ned:~ >ls
proj3  test  test.txt  txt.tset
<ned:~ >cat txt.tset

?nam niam ym pu tahW

.nanahcuB ydal yM<ned:~ >
<ned:~ >
<ned:~ >
<ned:~ >ls -r test
test.txt  temp2  dir1  1rid
<ned:~ >./proj3 test
<ned:~ >ls -r tset
txt.tset  dir1  2pmet  1rid
<ned:~ >
<ned:~ >
<ned:~ >./proj3 test.txt
Error: Can't create Bizzaro file or directory. ./txt.tset already exists.
<ned:~ >./proj3 test
Error: Can't create Bizzaro file or directory. ./tset already exists.


