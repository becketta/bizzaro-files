all: flipflopper.cpp support.h support.cpp
	g++ -ggdb -std=c++11 -lpthread -o proj3 flipflopper.cpp support.cpp

clean:
	rm -rf *.o
