/* ------------------------------------------------------------------
 * file name: flipflopper.cpp    a.k.a   Bizzaro
 *
 *      The flipflopper takes a local file or directory as input 
 *      and recursively inverts the the contents and name of all
 *      directories and regular files in the directory tree.
 *
 * author: Aaron Beckett
 *   date: 04/10/2015
 * ------------------------------------------------------------------
 */
#include <iostream>

#include "support.h"

using namespace std;


int main(int argc, char *argv[])
{
    // Check CL args
    //
    // Make sure the number of parameters is correct
    if (argc != 2) {
        cout << "Invalid number of parameters." << endl;
        cout << "Usage: $ ./proj3 <filename> || <dirname>" << endl;
        exit(1);
    }

    // Make sure the given file/directory name can be accessed
    if ( !canAccess(argv[1]) ) {
        cout << "Error: The directory or file you specified does not exist." << endl;
        handle_error(errno, "access");
    }

    // Parse the file/dir name argument
    string base_path = getBasePath(argv[1]);
    if (base_path.empty()) { base_path = "."; }
    string filename = getFileName(argv[1]);
    string bizzname = base_path + "/" + sinvert(filename);

    // Make sure there is not already a bizzaro copy of the file or directory
    if ( canAccess(bizzname) ) {
        cout << "Error: Can't create Bizzaro file or directory. ";
        cout << bizzname << " already exists." << endl;
        exit(1);
    }

    // Make the appropriate bizzaro file/directory
    typ t = filetype(argv[1]);
    switch(t) {
        case REGULAR_FILE:
            errno = 0;
            rename(argv[1], bizzname.c_str());
            if (errno != 0) { handle_error(errno, "rename"); }
            if ( !finvert(bizzname) ) {
                cout << "Error: Couldn't reverse the conents of the file." << endl;
                exit(1);
            }
            break;
        case DIRECTORY:
            errno = 0;
            rename(argv[1], bizzname.c_str());
            if (errno != 0) { handle_error(errno, "rename"); }
            if ( !dinvert(bizzname) ) {
                cout << "Error: Couldn't reverse the contents of the directory." << endl;
                exit(1);
            }
            break;
        case OTHER:
            cout << "That's not a regular file or directory." << endl;
            break;
        case NONE:
            handle_error(errno,"stat");
            break;
    }
}


