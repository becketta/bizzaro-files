#ifndef SUPPORT_H
#define SUPPORT_H
/* ------------------------------------------------------------------
 * file name: support.h
 *
 *      Declares custom data structures and functions to be used by
 *      flipflopper.cpp, otherwise known as Bizzaro.
 *
 * author: Aaron Beckett
 *   date: 04/10/2015
 * ------------------------------------------------------------------
 */
#include <string>
#include <cstring>
#include <cerrno>


#define handle_error(er, msg) \
    do { errno = er; perror(msg); exit(EXIT_FAILURE); } while (0)


enum typ { REGULAR_FILE, DIRECTORY, OTHER, NONE };

struct swapPair {
    std::string old_name;
    std::string new_name;
    bool mirrored;
    
    public:
        swapPair(const std::string& o, const std::string& n, bool m = false):
            old_name(o),
            new_name(n),
            mirrored(m) {}
};

/*
 * Custom Functions
 */
bool dinvert( const std::string&);
bool finvert( const std::string&);
std::string sinvert( const std::string& );
typ filetype(const std::string&);
std::string getFileName(const std::string&);
std::string getBasePath(const std::string&);
bool canAccess(const std::string&);

/*
 * Shared Data for Threads
 */
 
/*
 * Custom Data Structs for Threads
 */

#endif

